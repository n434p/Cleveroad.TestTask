﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Cleveroad.JWTService.Models;
using Cleveroad.JWTService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cleveroad.JWTService.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly UserContext usersContext;
        private readonly IPasswordService passwordService;
        private readonly ITokenService tokenService;

        public AccountController(UserContext context, IPasswordService passwordService, ITokenService tokenService)
        {
            usersContext = context;
            this.passwordService = passwordService;
            this.tokenService = tokenService;
        }

        /// <summary>
        /// Register new user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost("Register"), AllowAnonymous]
        public async Task<IActionResult> Register(string email, string password)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = usersContext.Users.SingleOrDefault(u => u.Id.ToString() == userId && u.Email == email);

            if (user != null)
                return StatusCode(422);

            user = new User
            {
                Email = email,
                Password = passwordService.GeneratePasswordHash(password)
            };

            usersContext.Users.Add(user);
            await usersContext.SaveChangesAsync();

            return Ok("Operation sucessful!");
        }

        /// <summary>
        /// Login user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost("Login"), AllowAnonymous]
        public async Task<IActionResult> Login(string email, string password)
        {
            var user = usersContext.Users.SingleOrDefault(u => u.Email == email);

            if (user == null || !passwordService.VerifyPassword(password, user.Password))
                return BadRequest();

            return await GetNewTokens(user);
        }

        /// <summary>
        /// Refresh user access token
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        [HttpPost("RefreshToken")]
        public async Task<IActionResult> RefreshToken(string refreshToken)
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = usersContext.Users.SingleOrDefault(u => u.Id.ToString() == userId && u.RefreshToken == refreshToken);

            if (user == null)
                return BadRequest();

            return await GetNewTokens(user);
        }

        /// <summary>
        /// Clears user tokens
        /// </summary>
        /// <returns></returns>
        [HttpDelete("Logout")]
        public async Task<IActionResult> Logout()
        {
            var userId = HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = usersContext.Users.SingleOrDefault(u => u.Id.ToString() == userId);

            if (user == null)
                return BadRequest();

            user.RefreshToken = null;

            await usersContext.SaveChangesAsync();

            return NoContent();
        }


        /// <summary>
        /// Checks user authorization state
        /// </summary>
        /// <returns></returns>
        [HttpPost("Check")]
        public async Task<IActionResult> Check()
        {
            return Ok();
        }

        private async Task<ObjectResult> GetNewTokens(User user)
        {
            var usersClaims = new[]
{
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var newJwtToken = tokenService.GenerateAccessToken(usersClaims);
            var newRefreshToken = tokenService.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            await usersContext.SaveChangesAsync();

            return new ObjectResult(new
            {
                token = newJwtToken,
                refreshToken = newRefreshToken
            });
        }
    }
}
