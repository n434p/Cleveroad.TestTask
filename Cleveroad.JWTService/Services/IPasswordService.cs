﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cleveroad.JWTService.Services
{
    public interface IPasswordService
    {
        string GeneratePasswordHash(string password);
        bool VerifyPassword(string password, string passwordHash);
    }
}
