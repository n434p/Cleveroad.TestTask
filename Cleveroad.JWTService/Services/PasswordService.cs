﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cleveroad.JWTService.Services
{
    public class PasswordService : IPasswordService
    {
        public string GeneratePasswordHash(string password)
        {
            return new string(password.Reverse().ToArray());
        }

        public bool VerifyPassword(string password, string passwordHash)
        {
            return password == new string(passwordHash.Reverse().ToArray());
        }
    }
}
