﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cleveroad.JWTService.Models
{
    public class UserContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public UserContext() : base()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=users.sqlite");
        }
    }
}
